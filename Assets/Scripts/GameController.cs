﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
public class GameController : MonoBehaviour
{
    public UnityAction<bool> OnCompleted;
    public UnityAction OnReset;
    public static GameController instance;
    [SerializeField] Text hudText;
    int _gearsTotal = 0;
    int _gearsOnPos = 0;
    private void Awake() 
    {
        instance = this;
    }
    public void GearInPlace(bool placed)
    {
        if(placed)
        {
            _gearsOnPos += 1;
            if(_gearsOnPos == _gearsTotal)
            {
                OnCompleted(true);
                hudText.text = "YAY, PARABÉNS,\nTASK CONCLUÍDA!";
            }
        }
        else
        {
            if(_gearsOnPos == _gearsTotal)
            {
                OnCompleted(false);
                hudText.text = "ENCAIXE AS ENGRENAGENS EM QUALQUER ORDEM!";
            }
             _gearsOnPos -= 1;             
        }
    }
    public void GearCount()
    {
        _gearsTotal += 1;
    }
    public void ResetGame()
    {
        OnReset();
    }
}
