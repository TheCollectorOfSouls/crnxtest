using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
public class Gear : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    [SerializeField]private GameObject gearUI;
    [SerializeField]private GameObject gearWorld;
    [SerializeField]private Canvas canvas;
    [SerializeField]private Slot starterSlot;
    Animator _gearAnim;
    CanvasGroup _canvasGroup;
    Slot _currentSlot;
    // Start is called before the first frame update
    void Start()
    {
        _canvasGroup = gearUI.GetComponent<CanvasGroup>();
        _currentSlot = starterSlot;
        _currentSlot.GearPlaced();
        GameController.instance.GearCount();
        GameController.instance.OnCompleted += Rotate;
        GameController.instance.OnReset += ResetPosition;
        _gearAnim = gearWorld.GetComponent<Animator>();
    }
    public void OnBeginDrag(PointerEventData eventData)
    {
        _canvasGroup.blocksRaycasts = false;
        SwitchState(true);
        transform.SetParent(canvas.transform);
        transform.localScale = Vector3.one;
    }
    public void OnDrag(PointerEventData eventData)
    {
        transform.position = eventData.position;
    }
    public void OnEndDrag(PointerEventData eventData)
    {
        _canvasGroup.blocksRaycasts = true;
        MoveToParentSlot();
        SwitchState(false);
    }
    public void ChangeSlot(Slot slot)
    {
        _currentSlot.GearRemoved();
        _currentSlot = slot;
        _currentSlot.GearPlaced();
    }
    //Changes which gear is active
    void SwitchState(bool leavingSlot)
    {
        if(!_currentSlot.isUI)
        {
            gearUI.SetActive(leavingSlot);
            gearWorld.SetActive(!leavingSlot);
            GameController.instance.GearInPlace(!leavingSlot);
        }
    }
    void MoveToParentSlot()
    {
        transform.SetParent(_currentSlot.transform);
        transform.localScale = Vector3.one;
        transform.localPosition = Vector2.zero;
    }
    void Rotate(bool toggle)
    {
        if(toggle)
        {
            if(_currentSlot.isReverse)
            {
                _gearAnim.SetFloat("RotateSpeed", -1);
            }
            else
            {
                _gearAnim.SetFloat("RotateSpeed", 1);
            }
            _gearAnim.SetBool("Rotate", true);
        }
        else
        {
            _gearAnim.SetBool("Rotate", false);
        }
    }
    void ResetPosition()
    {
        if(_currentSlot != starterSlot)
        {
            SwitchState(true);
            ChangeSlot(starterSlot);
        }
        MoveToParentSlot();
    }
    private void OnDestroy() 
    {
        GameController.instance.OnCompleted -= Rotate;
        GameController.instance.OnReset -= ResetPosition;
    }
}
