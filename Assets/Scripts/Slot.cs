﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Slot : MonoBehaviour, IDropHandler
{
    [SerializeField] bool uiSlot;
    [SerializeField] bool reverse;
    bool _occupied = false;
    public bool isOccupied{get {return _occupied;}}
    public bool isReverse{get {return reverse;}}
    public bool isUI{get {return uiSlot;}}
    public void OnDrop(PointerEventData eventData)
    {
        if(!_occupied)
        {
            if(eventData.pointerDrag != null)
            {
                Gear gear = eventData.pointerDrag.GetComponent<Gear>();
                if(gear != null)
                {
                    gear.ChangeSlot(this);
                }
            }
        }
    }
    public void GearRemoved()
    {
        _occupied = false;
    }
    public void GearPlaced()
    {
        _occupied = true;
    }
}
